class AddSlugToEventis < ActiveRecord::Migration
  def change
    add_column :eventis, :slug, :string
    add_index :eventis, :slug, unique:true
  end
end
