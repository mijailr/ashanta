class CreateEventis < ActiveRecord::Migration
  def change
    create_table :eventis do |t|
      t.string :title
      t.text :description
      t.string :location
      t.date :date

      t.timestamps
    end
  end
end
