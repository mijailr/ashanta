class EventiController < ApplicationController
def index
	@custitle="Ashanta Yoga Urbino"
	respond_to do |format|
        format.html
    end
end
def show
	@custitle="Yoga Eventis | Ashtanga Yoga Urbino"
	@e=Eventi.page(params[:page]).per(5).order('created_at DESC')
	respond_to do |f|
		f.html
	end
	
end
def single
	@e=Eventi.find(params[:id])
	@custitle="#{@e.title} | Ashtanga Yoga Urbino"
	respond_to do |f|
		f.html
	end
end
def bed
	@custitle="Bed & Breakfast | Ashtanga Yoga Urbino"
	respond_to do |f|
		f.html
	end
end
def tradizzione
	@custitle="Tradizzione | Ashtanga Yoga Urbino"
	respond_to do |f|
		f.html
	end
end
def istruttore
	@custitle="Istruttore | Ashtanga Yoga Urbino"
	respond_to do |f|
		f.html
	end
end
def galleria
	@custitle="Galleria | Ashtanga Yoga Urbino"
	respond_to do |f|
		f.html
	end
end
def classi
	@custitle="Classi e Orari | Ashtanga Yoga Urbino"
	respond_to do |f|
		f.html
	end
end

end
