class Eventi < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  attr_accessible :date, :description, :location, :title, :slug

end
