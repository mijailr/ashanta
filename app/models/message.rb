# app/models/message.rb
class Message < MailForm::Base
  attribute :name,          :validate => true
  attribute :email,         :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message_title, :validate => true
  attribute :message_body,  :validate => true 

  def headers
    {
      :subject => "Un Mensaje",
      :to => "mijail.rondon@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
end
